var Discord = require('discord.io');
var auth = require('./auth.json');
var fetch = require('node-fetch');
var fs = require('fs');

var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

bot.on('message', function (user, userID, channelID, message, evt) {
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        switch (cmd) {
            case 'servers':
                console.log("Servers Requested")
                getExternalIp().then((ip) => bot.sendMessage({
                    to: channelID,
                    message: ' \n IP: ' + ip + ' \n Infinity Port: 25565 \n Creative Infinity Port: 25566 \n Terraria Port: 7777'
                }));

                break;
            case 'modlist':

                if (args[1] == 'add') {
                    let modName = parseString(args, 2);
                    addToModList(modName);
                } else if (args[1] == 'remove') {
                    let modName = parseString(args);
                    removeFromModList(modName).then(err => {
                        bot.sendMessage({
                            to: channelID,
                            message: err || 'Removed'
                        })
                    });
                } else {
                    sendModList(channelID);
                }
                break;
            case 'cleanup':
                console.log("Cleanup Requested")
                deleteMessages(bot, channelID, (err) => {
                    if (err)
                        bot.sendMessage({
                            to: channelID,
                            message: JSON.stringify(err)
                        });
                    else
                        bot.sendMessage({
                            to: channelID,
                            message: "Cleanup Completed"
                        });
                })
                break;
            case 'praise':
                bot.sendMessage({
                    to: channelID,
                    message: JSON.stringify("Praise the sun! \\[T]/").replace('"', '').replace('"', '')
                });
                break;
            case 'source':
                bot.sendMessage({
                    to: channelID,
                    message: "https://gitlab.com/deuce109/ip-bot"
                });
                break;
            case 'debug':
                bot.getMessage({
                    channelID: channelID,
                    messageID: bot.channels[channelID].last_message_id
                }, (err, message) => {
                    if (!err) {
                        bot.sendMessage({
                            to: channelID,
                            message: JSON.stringify(message)
                        });
                    }
                });

                break;
            case 'commands':
                bot.sendMessage({
                    to: channelID,
                    message: "!modlist: \n" +
                        "   add {modName}: Adds mod to modlist \n" +
                        "   delete {modName}: Deletes mod from modlist \n" +
                        "!servers: Gives ip and ports for servers \n" +
                        "!cleanup: Clears chat history except pinned messages \n" +
                        "!commands: Sends this message again \n" +
                        "!rng {max number}: Gives a number between 0 and input number inclusive \n" +
                        "!decide {list of choices}: Randomly chooses one of the given choices"
                });
                break;
            case 'everythingworks':
                bot.sendMessage({
                    to: channelID,
                    message: "Yay!"
                });
                break;
            case 'rng':
                bot.sendMessage(
                    {
                        to: channelID,
                        message: rng(args[1])
                    }
                );
                break;
            
            case 'usernames':
                    switch (args[1])
                    {
                        case 'register':
                            addToUserList(user, args[args.length -1], parseString(args, 2))
                        break;

                        case 'remove':
                            
                        break;

                        default:
                            bot.sendMessage({
                                to: channelID,
                                message : retrieveUserList(args[1])
                            });
                    }
                
                break;
            case 'decide':
                args = args.splice(1);
                bot.sendMessage(
                    {
                        to:channelID,
                        message: (random(args))
                    }
                )
                break;
        }

    }
});



async function getExternalIp() {
    return fetch('http://myexternalip.com/raw').then(response => response.text());;
}

function deleteMessages(bot, channelID, callback) {

    console.log("Delete started");
    bot.getMessages({
        channelID: channelID
    }, (err, messageArray) => {
        let messageIds = [];
        if (!err) {
            messageArray.map(x => {
                if (!x.pinned) {
                    messageIds.push(x.id);
                }
            })
            bot.deleteMessages({
                channelID: channelID,
                messageIDs: messageIds
            }, () => {});
        } else
            console.log(err);
        console.log("Delete finished")
        callback(err);
    });

}

function addToUserList(discordName, username, service, channelID)
{
    console.log(`Adding username ${username} for ${service} for user ${discordName}`);

    let json = fs.readFileSync('./usernames.json', 'utf-8');
    var written =false;
    var message = '';
    var users = JSON.parse(json);

    users.map((user, x) =>
        {
            if (user.discordName == discordName)
            {
                console.log(user);
                user.registeredNames.map( (registeredName , y) => 
                    {
                        console.log(registeredName);
                        
                        if (registeredName.service == service)
                        {
                            console.log('overwritting');
                            users[x].registeredNames[y].username = username;
                            written = true;
                            message = "Username updated";
                        }
                    }
                )
                if (!written)
                {
                    console.log('adding');
                    users[x].registeredNames.push({username: username, service: service});
                    written = true;
                    message = "Username added";        
                }
            }
        }
    )

    if (!written)
    {
        console.log('adding user');
        users.push(
            {
                discordName: discordName,
                registeredNames: [
                    {
                    username: username,
                    service: service
                    }
                ]
            }
        );
        message = "User and username added"
    }
    let newJson = JSON.stringify(users);

    fs.writeFile('usernames.json', newJson, ()=>{});

    bot.sendMessage(
        {
            to: channelID,
            message: message
        }
    );
}

function retrieveUserList(discordName, channelID)
{
    console.log(`Getting usernames of ${discordName}`);
    let json = fs.readFileSync('usernames.json', 'utf-8');
    var users = JSON.parse(json);
    var names = [];
    users.map((user, i) => 
    {
        console.log(user);
        if (discordName = user.discordName)
        {
            user.registeredNames.map((registeredName) => 
            {
                names.push(`${registeredName.service}: ${registeredName.username}`);
            })
            
        }
    }
    );

    bot.sendMessage(
        {
            to: channelID, 
            message: `Usernames for ${discordName} \n ${names.join('\n')}`
        }
    );
}

function removeFromUserList(discordName, username, service)
{

}

async function sendModList(channelID) {
    fs.readFile('modlist.txt', 'utf8', (err, mods) => {
        err ? console.log(err) :
            modlist = mods.split('\n');
        modlist.sort((a, b) => {
            if (a < b) return -1;
            if (a > b) return 1;
            return 0;
        })
        bot.sendMessage({
            to: channelID,
            message: modlist.join('\n') || 'None'
        })
    });
}

async function addToModList(mod, channelID) {


    fs.readFile('modlist.txt', 'utf8', (err, mods) => {

        let alreadyAdded = false;

        if (mods) {
            mods = mods.split('\n');
            console.log(mods);
            for (i = 0; i < mods.length; i++) {
                alreadyAdded = mods[i] == mod;
                if (alreadyAdded)
                    break;
            }

        }

        console.log(alreadyAdded);

        if (!alreadyAdded) {
            fs.appendFile('modlist.txt', mod + '\n', (err) => {
                if (err) console.log(err);
            });
        }

        bot.sendMessage({
            to: channelID,
            message: err ? err : alreadyAdded ? "Already Added" : "Added"
        });
    });
}

async function removeFromModList(mod) {
    fs.readFile('modlist.txt', 'utf8', (err, mods) => {
        err ? console.log(err) :
            mods = mods.replace(mod + '\n', '');
        fs.unlink('modlist.txt', (err) => {
            return err
        });
        fs.writeFile('modlist.txt', mods, (err) => {
            return err
        });
    })
}

function parseString(args , defaultIndex) {
    var commandString = args.join(' ');
    var commandResult = commandString.match(/".*"/);
    commandResult = commandResult ? commandResult[0] : args[defaultIndex];
    commandResult = commandResult.replace('"', '').replace('"', '');
    return commandResult;
}

function rng(maxNumber)
{
    return Math.round((Math.random() * maxNumber) +1 );
}

function random(dict)
{
    return dict[Math.round((Math.random() * dict.length))];
}